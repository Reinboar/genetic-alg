class Gene
  attr_reader :value

  def initialize
    generate_new
  end

  def generate_new
    @value = nil
  end

  def mutate_value
    
  end

  def to_s

  end

end

class Chromosome
  
  attr_reader :genes, :mutation_rate

  def initialize(num_genes, mutation_rate)
    @genes = Array.new(num_genes)
    @mutation_rate = mutation_rate
  end

  def mutate
    @genes.each do |g| 
      g.mutate_value if rand(100) < @mutation_rate
    end
    return self
  end

  def crossover(mate)

  end

  def to_s
    @genes.each { |g| g.to_s }.join("")
  end

end

class Population
  attr_reader :chroms

  def initialize(num_chroms, num_genes, mutation_rate)
    
  end

  def determine_fitness(chrom)
    
  end

  def process_generation
    
  end

end
